import java.awt.*;
import javax.swing.*;
import java.util.List;
import java.util.Collections;
import java.awt.event.ActionEvent;

public class MahjongMenuBar extends JMenuBar
{
    private MahjongBoard board;

    private JMenuItem playGameMenuItem     = new JMenuItem("Play");
    private JMenuItem restartGameMenuItem  = new JMenuItem("Restart");
    private JMenuItem numberedGameMenuItem = new JMenuItem("Numbered Game");
    private JMenuItem removedTilesMenuItem = new JMenuItem("Removed Tiles");
    private JMenuItem soundOnMenuItem      = new JMenuItem("On");
    private JMenuItem soundOffMenuItem     = new JMenuItem("Off");
    private JMenuItem undoMoveMenuItem     = new JMenuItem("Undo");

    public MahjongMenuBar(MahjongBoard board)
    {
        setBoard(board);
        initializeMenu();
    }

    private void initializeMenu()
    {
        JMenu gameMenu    = new JMenu("Game");
        JMenu soundMenu   = new JMenu("Sound");
        JMenu moveMenu    = new JMenu("Move");
        JMenu helpMenu    = new JMenu("Help");

        JMenuItem tournamentModeMenuItem = new JMenuItem("Tournament Mode");
        JMenuItem exitMenuItem           = new JMenuItem("Exit");

        JMenuItem operationMenuItem    = new JMenuItem("Operation");
        JMenuItem gameRulesMenuItem    = new JMenuItem("Game Rules");

        playGameMenuItem.addActionListener(this::playGameMenuItemClick);
        numberedGameMenuItem.addActionListener(this::numberedGameMenuItemClick);
        restartGameMenuItem.addActionListener(this::restartGameMenuItemClick);
        removedTilesMenuItem.addActionListener(this::removedTilesMenuItemClick);
        tournamentModeMenuItem.addActionListener(this::tournamentModeMenuItemClick);
        exitMenuItem.addActionListener(this::exitMenuItemClick);
        soundOnMenuItem.addActionListener(this::soundOnMenuItemClick);
        soundOffMenuItem.addActionListener(this::soundOffMenuItemClick);
        undoMoveMenuItem.addActionListener(this::undoMoveMenuItemClick);
        operationMenuItem.addActionListener(this::operationMenuItemClick);
        gameRulesMenuItem.addActionListener(this::gameRulesMenuItemClick);

        playGameMenuItem.setToolTipText("Start your Mahjong game");
        restartGameMenuItem.setToolTipText("Restart your game with the same (or new) Seed");
        numberedGameMenuItem.setToolTipText("Start a new game with a specific Seed");
        tournamentModeMenuItem.setToolTipText("Start a new game in Tournament Mode");
        removedTilesMenuItem.setToolTipText("View all Tiles that have been removed");
        exitMenuItem.setToolTipText("Exit the game");
        soundOnMenuItem.setToolTipText("Enable game sounds");
        soundOffMenuItem.setToolTipText("Disable game sounds");
        undoMoveMenuItem.setToolTipText("Undo your last move");
        operationMenuItem.setToolTipText("View useful information about game operations");
        gameRulesMenuItem.setToolTipText("View useful information about the game rules");

        gameMenu.add(playGameMenuItem);
        gameMenu.add(restartGameMenuItem);
        gameMenu.add(numberedGameMenuItem);
        gameMenu.add(tournamentModeMenuItem);
        gameMenu.add(new JSeparator());
        gameMenu.add(removedTilesMenuItem);
        gameMenu.add(new JSeparator());
        gameMenu.add(exitMenuItem);
        soundMenu.add(soundOnMenuItem);
        soundMenu.add(soundOffMenuItem);
        moveMenu.add(undoMoveMenuItem);
        helpMenu.add(operationMenuItem);
        helpMenu.add(gameRulesMenuItem);

        add(gameMenu);
        add(soundMenu);
        add(moveMenu);
        add(helpMenu);
    }

    private void setBoard(MahjongBoard board)
    {
        this.board = board;
    }

    private void playGameMenuItemClick(ActionEvent actionEvent)
    {
        board.drawMahjongBoard();
        if (board.getSoundStatus())
        {
            PlayClip clip = new PlayClip("/audio/new_game.wav");
            clip.play();
        }

        playGameMenuItem.setEnabled(false);
    }

    private void restartGameMenuItemClick(ActionEvent actionEvent)
    {
        int dialog = JOptionPane.showConfirmDialog(board.getPanel(),
                "Would you like to start a game with a new seed (YES), or the same seed (NO)?\nClick CANCEL to return to your game.",
                "Restart Game?", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (dialog == 0 || dialog == 1)
        {
            board.clearTiles();
            if (dialog == 0)
            {
                board.setSeed((int)(System.currentTimeMillis() % 1000000));
            }
            board.drawMahjongBoard();

            if (board.getSoundStatus())
            {
                PlayClip clip = new PlayClip("/audio/new_game.wav");
                clip.play();
            }
        }
    }

    private void numberedGameMenuItemClick(ActionEvent actionEvent)
    {
        String response = JOptionPane.showInputDialog(board.getPanel(), "Enter Seed:", "Numbered Game", JOptionPane.QUESTION_MESSAGE);
        if (response != null)
        {
            board.clearTiles();
            try
            {
                board.setSeed(Integer.parseInt(response));
                board.drawMahjongBoard();
            }
            catch (NumberFormatException nfe)
            {
                JOptionPane.showMessageDialog(board.getPanel(), "Please enter a number!", "Not A Number", JOptionPane.ERROR_MESSAGE);
                numberedGameMenuItemClick(null);
            }

        }

        if (board.getSoundStatus())
        {
            PlayClip clip = new PlayClip("/audio/new_game.wav");
            clip.play();
        }
    }

    private void removedTilesMenuItemClick(ActionEvent actionEvent)
    {
        List<Tile> usedTiles = board.getUsedTiles();
        Collections.reverse(usedTiles);

        JPanel panel = new JPanel();

        for (Tile t : usedTiles)
        {
            panel.add(t);
            t.removeMouseListener(board);
        }

        JScrollPane scrollPane = new JScrollPane(panel);
        scrollPane.setPreferredSize(new Dimension(400, 140));

        JOptionPane.showConfirmDialog(board.getPanel(), scrollPane, "Removed Tiles", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE);

    }

    private void tournamentModeMenuItemClick(ActionEvent actionEvent)
    {
        int dialog = JOptionPane.showConfirmDialog(board.getPanel(), "Do you want to play in Tournament Mode? The following changes will be made:\n" +
                                                                              "- Cannot restart the game, or start a Numbered game\n" +
                                                                              "- Cannot undo any moves\n" +
                                                                              "- Cannot view removed Tiles\n" +
                                                                              "- The current score and elapsed time will be added to the title bar", "Enable Tournament Mode?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (dialog == 0)
        {
            restartGameMenuItem.setEnabled(false);
            numberedGameMenuItem.setEnabled(false);
            removedTilesMenuItem.setEnabled(false);
            undoMoveMenuItem.setEnabled(false);

            board.setSeed((int)System.currentTimeMillis() % 1000000);
            board.clearTiles();
            board.drawMahjongBoard();
            board.setTournamentMode(true);
            playGameMenuItem.setEnabled(false);

            JLabel timer = new Timer();
            timer.setSize(new Dimension(200, 50));
            timer.setLocation(new Point(15, board.getPanel().getHeight() - 50));
            board.getPanel().add(timer);
            board.getPanel().repaint();

            if (board.getSoundStatus())
            {
                PlayClip clip = new PlayClip("/audio/new_game.wav");
                clip.play();
            }
            JOptionPane.showMessageDialog(board.getPanel(), "Tournament Mode has been enabled!", "Tournament Mode Enabled", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void exitMenuItemClick(ActionEvent actionEvent)
    {
        int dialog = JOptionPane.showConfirmDialog(board.getPanel(), "Are you sure you want to quit?", "Quit Game?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (dialog == 0)
        {
            if (board.getSoundStatus())
            {
                PlayClip clip = new PlayClip("/audio/closing.wav");
                clip.play();
            }
            System.exit(0);
        }
    }

    private void undoMoveMenuItemClick(ActionEvent actionEvent)
    {
        List<Tile> tiles = board.getTiles();
        List<Tile> usedTiles = board.getUsedTiles();

        if (usedTiles.size() > 0)
        {
            ImagePanel panel = board.getPanel();

            Tile firstTile = usedTiles.get(usedTiles.size() - 1);
            Tile secondTile = usedTiles.get(usedTiles.size() - 2);

            usedTiles.remove(firstTile);
            usedTiles.remove(secondTile);

            firstTile.addMouseListener(board);
            firstTile.addMouseListener(board);

            firstTile.setLocation(new Point(firstTile.getXPoint(), firstTile.getYPoint()));
            secondTile.setLocation(new Point(secondTile.getXPoint(), secondTile.getYPoint()));

            tiles.add(firstTile);
            tiles.add(secondTile);

            panel.add(tiles.get(tiles.indexOf(secondTile)), tiles.get(tiles.indexOf(secondTile)).getZOrder());
            panel.add(tiles.get(tiles.indexOf(firstTile)), tiles.get(tiles.indexOf(firstTile)).getZOrder());

            panel.repaint();

            if (board.getSoundStatus())
            {
                PlayClip clip = new PlayClip("/audio/undo.wav");
                clip.play();
            }
        }
    }

    private void soundOnMenuItemClick(ActionEvent actionEvent)
    {
        board.setSoundStatus(true);
        soundOnMenuItem.setEnabled(false);
        soundOffMenuItem.setEnabled(true);

        PlayClip clip = new PlayClip("/audio/sound_toggle.wav");
        clip.play();
    }

    private void soundOffMenuItemClick(ActionEvent actionEvent)
    {
        board.setSoundStatus(false);
        soundOnMenuItem.setEnabled(true);
        soundOffMenuItem.setEnabled(false);

        PlayClip clip = new PlayClip("/audio/sound_toggle.wav");
        clip.play();
    }

    private void operationMenuItemClick(ActionEvent actionEvent)
    {
        Help help = new Help("help/operations.html", "Game Operations");
        help.display();
    }

    private void gameRulesMenuItemClick(ActionEvent actionEvent)
    {
        Help help = new Help("help/rules.html", "Game Rules");
        help.display();
    }
}
