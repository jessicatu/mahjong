import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;
import java.awt.FlowLayout;



public class CircleTile extends RankTile
{
	private final Circle circles[] = new Circle[9];

	public CircleTile(int rank)
	{
		super(rank);
		setToolTipText(toString());
		switch (rank)
		{
			case 1:
				circles[0] = new Pancake(49, 47, Color.RED);
				break;
			case 2:
				circles[0] = new Circle(48, 35, CustomGreen);
				circles[1] = new Circle(48, 60, Color.RED);
				break;
			case 3:
				circles[0] = new Circle(25, 23, Color.BLUE);
				circles[1] = new Circle(49, 47, Color.RED);
				circles[2] = new Circle(72, 70, CustomGreen);
				break;
			case 4:
				circles[0] = new Circle(33, 34, Color.BLUE);
				circles[1] = new Circle(33, 64, CustomGreen);
				circles[2] = new Circle(63, 34, CustomGreen);
				circles[3] = new Circle(63, 64, Color.BLUE);
				break;
			case 5:
				circles[0] = new Circle(25, 25, Color.BLUE);
				circles[1] = new Circle(49, 49, Color.RED);
				circles[2] = new Circle(72, 72, Color.BLUE);
				circles[3] = new Circle(72, 25, CustomGreen);
				circles[4] = new Circle(25, 72, CustomGreen);
				break;
			case 6:
				circles[0] = new Circle(33, 25, CustomGreen);
				circles[1] = new Circle(63, 25, CustomGreen);
				circles[2] = new Circle(33, 49, Color.RED);
				circles[3] = new Circle(63, 49, Color.RED);
				circles[4] = new Circle(33, 72, Color.RED);
				circles[5] = new Circle(63, 72, Color.RED);
				break;
			case 7:
				circles[0] = new Circle(25, 25, CustomGreen);
				circles[1] = new Circle(49, 35, CustomGreen);
				circles[2] = new Circle(72, 47, CustomGreen);
				circles[3] = new Circle(33, 58, Color.RED);
				circles[4] = new Circle(60, 58, Color.RED);
				circles[5] = new Circle(33, 72, Color.RED);
				circles[6] = new Circle(60, 72, Color.RED);
				break;
			case 8:
				circles[0] = new Circle(33, 22, Color.BLUE);
				circles[1] = new Circle(63, 22, Color.BLUE);
				circles[2] = new Circle(33, 40, Color.BLUE);
				circles[3] = new Circle(63, 40, Color.BLUE);
				circles[4] = new Circle(33, 58, Color.BLUE);
				circles[5] = new Circle(63, 58, Color.BLUE);
				circles[6] = new Circle(63, 75, Color.BLUE);
				circles[7] = new Circle(33, 75, Color.BLUE);
				break;
			case 9:
				circles[0] = new Circle(25, 25, CustomGreen);
				circles[1] = new Circle(49, 25, CustomGreen);
				circles[2] = new Circle(72, 25, CustomGreen);
				circles[3] = new Circle(25, 49, Color.RED);
				circles[4] = new Circle(49, 49, Color.RED);
				circles[5] = new Circle(72, 49, Color.RED);
				circles[6] = new Circle(25, 72, Color.BLUE);
				circles[7] = new Circle(49, 72, Color.BLUE);
				circles[8] = new Circle(72, 72, Color.BLUE);
				break;
		}
	}

	public static void main(String[] args)
	{
		JFrame jframe = new JFrame();

		jframe.setLayout(new FlowLayout());
		jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		jframe.setTitle("Circle Tiles");

		jframe.add(new CircleTile(1));
		jframe.add(new CircleTile(2));
		jframe.add(new CircleTile(3));
		jframe.add(new CircleTile(4));
		jframe.add(new CircleTile(5));
		jframe.add(new CircleTile(6));
		jframe.add(new CircleTile(7));
		jframe.add(new CircleTile(8));
		jframe.add(new CircleTile(9));

		jframe.pack();
		jframe.setVisible(true);
	}

	@Override
	public String toString()
	{
		return "Circle " + rank ;
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		for (Circle c : circles)
		{
			if (c != null)
			{
				c.draw(g);
			}
		}
	}
}
