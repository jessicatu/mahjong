import java.awt.*;
import javax.swing.*;

public class BambooTile extends RankTile
{
	public BambooTile(int rank)
	{
		super(rank);
		setToolTipText(toString());
	}

	public static void main(String[] args)
	{
		JFrame jframe = new JFrame();

		jframe.setLayout(new FlowLayout());
		jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		jframe.setTitle("Bamboo Tiles");

		jframe.add(new BambooTile(2));
		jframe.add(new BambooTile(3));
		jframe.add(new BambooTile(4));
		jframe.add(new BambooTile(5));
		jframe.add(new BambooTile(6));
		jframe.add(new BambooTile(7));
		jframe.add(new BambooTile(8));
		jframe.add(new BambooTile(9));

		jframe.pack();
		jframe.setVisible(true);
	}

	private void drawBamboo(Graphics g, int x, int y, Color c)
	{
		g.setColor(c);
		g.fillOval(x, y, 10, 5);
		g.fillOval(x, y + 16, 10, 5);
		g.fillRoundRect(x + 2, y + 1, 6, 20, 2, 2);
		g.setColor(Color.WHITE);
		g.drawLine(x + 5, y + 2, x + 5, y + 20);
		g.setColor(c);
		g.fillOval(x, y + 8, 10, 5);
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		switch (rank)
		{
			case 2:
				drawBamboo(g, 49, 33, Color.BLUE);
				drawBamboo(g, 49, 54, CustomGreen);
				break;
			case 3:
				drawBamboo(g, 49, 33, Color.BLUE);
				drawBamboo(g, 35, 54, CustomGreen);
				drawBamboo(g, 63, 54, CustomGreen);
				break;
			case 4:
				drawBamboo(g, 35, 33, Color.BLUE);
				drawBamboo(g, 35, 54, CustomGreen);
				drawBamboo(g, 63, 33, CustomGreen);
				drawBamboo(g, 63, 54, Color.BLUE);
				break;
			case 5:
				drawBamboo(g, 28, 33, CustomGreen);
				drawBamboo(g, 28, 54, Color.BLUE);
				drawBamboo(g, 49, 45, Color.RED);
				drawBamboo(g, 70, 33, Color.BLUE);
				drawBamboo(g, 70, 54, CustomGreen);
				break;
			case 6:
				drawBamboo(g, 28, 33, CustomGreen);
				drawBamboo(g, 28, 54, Color.BLUE);
				drawBamboo(g, 49, 33, CustomGreen);
				drawBamboo(g, 49, 54, Color.BLUE);
				drawBamboo(g, 70, 33, CustomGreen);
				drawBamboo(g, 70, 54, Color.BLUE);
				break;
			case 7:
				drawBamboo(g, 49, 23, Color.RED);
				drawBamboo(g, 28, 45, CustomGreen);
				drawBamboo(g, 28, 66, Color.BLUE);
				drawBamboo(g, 49, 45, CustomGreen);
				drawBamboo(g, 49, 66, Color.BLUE);
				drawBamboo(g, 70, 45, CustomGreen);
				drawBamboo(g, 70, 66, Color.BLUE);
				break;
			case 8:
				drawBamboo(g, 28, 23, CustomGreen);
				drawBamboo(g, 28, 66, Color.BLUE);
				drawBamboo(g, 49, 23, CustomGreen);
				drawBamboo(g, 49, 66, Color.BLUE);
				drawBamboo(g, 70, 23, CustomGreen);
				drawBamboo(g, 70, 66, Color.BLUE);
				drawBamboo(g, 35, 44, Color.RED);
				drawBamboo(g, 63, 44, Color.RED);
				break;
			case 9:
				drawBamboo(g, 28, 23, Color.RED);
				drawBamboo(g, 28, 44, Color.RED);
				drawBamboo(g, 49, 23, Color.BLUE);
				drawBamboo(g, 49, 44, Color.BLUE);
				drawBamboo(g, 70, 23, CustomGreen);
				drawBamboo(g, 70, 44, CustomGreen);
				drawBamboo(g, 28, 66, Color.RED);
				drawBamboo(g, 49, 66, Color.BLUE);
				drawBamboo(g, 70, 66, CustomGreen);
				break;
		}
	}

	@Override
	public String toString()
	{
		return "Bamboo " + rank;
	}
}