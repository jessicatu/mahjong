import java.awt.*;
import javax.swing.*;
import java.awt.event.MouseListener;
import java.util.HashMap;

public class Tile extends JPanel
{
	private int x, y, layer, zOrder;

	private 	final static Color 		DarkGreen  		= new Color(0, 139, 0);
	private 	final static Color 		LightGreen 		= new Color(0, 210, 0);
	private 	final static Color		TopColor		= new Color(255, 231, 186);
	private 	final static Color		TopLightColor	= new Color(255, 250, 240);
	public 		final static Color 		CustomGreen  	= new Color(0, 205, 0);
	private		final static Dimension	WindowSize 		= new Dimension(91, 91);

	private static int[] xCoordinates 		= {0, 0, 10, 10};
	private static int[] yCoordinates 		= {70, 0, 10, 80};
	
	private static int[] xInnerCoordinates 	= {10, 10, 20, 20};
	private static int[] yInnerCoordinates 	= {80, 10, 20, 90};
	
	private static int[] xTopCoordinates 	= {70, 0, 10, 80};
	private static int[] yTopCoordinates 	= {0, 0, 10, 10};
	
	private static int[] xTopInnerCoordinates 	= {80, 10, 20, 90};
	private static int[] yTopInnerCoordinates 	= {10, 10, 20, 20};
	
	private static Polygon sideTile, sideInnerTile, topTile, topInnerTile;
	
	protected static HashMap<String, String> chineseUniChars = new HashMap<String, String>();
	
	static
	{
		sideTile 		= new Polygon(xCoordinates, yCoordinates, xCoordinates.length);
		sideInnerTile 	= new Polygon(xInnerCoordinates, yInnerCoordinates, xCoordinates.length);
		topTile 		= new Polygon(xTopCoordinates, yTopCoordinates, xCoordinates.length);
		topInnerTile 	= new Polygon(xTopInnerCoordinates, yTopInnerCoordinates, xCoordinates.length);
		
		chineseUniChars.put("1", "\u4E00");
		chineseUniChars.put("2", "\u4E8C");
		chineseUniChars.put("3", "\u4E09");
		chineseUniChars.put("4", "\u56DB");
		chineseUniChars.put("5", "\u4E94");
		chineseUniChars.put("6", "\u516D");
		chineseUniChars.put("7", "\u4E03");
		chineseUniChars.put("8", "\u516B");
		chineseUniChars.put("9", "\u4E5D");
		chineseUniChars.put("N", "\u5317");
		chineseUniChars.put("E", "\u6771");
		chineseUniChars.put("W", "\u897F");
		chineseUniChars.put("S", "\u5357");
		chineseUniChars.put("C", "\u4E2D");
		chineseUniChars.put("F", "\u767C");
		chineseUniChars.put("wan", "\u842C");
	}

	public Tile()
	{
		setPreferredSize(WindowSize);
		this.setBounds(100, 100, 91, 91);
	}

	public static void main(String[] args)
	{
		JFrame jframe = new JFrame();

		jframe.setLayout(new FlowLayout());

		jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		jframe.setTitle("Tile");
		jframe.add(new Tile());
		jframe.pack();
		jframe.setVisible(true);
	}

	public int getXPoint()
	{
		return this.x;
	}

	public int getYPoint()
	{
		return this.y;
	}

	public int getLayer()
	{
		return this.layer;
	}

	public int getZOrder()
	{
		return this.zOrder;
	}

	public void setLayer(int layer)
	{
		this.layer = layer;
	}

	public void setZOrder(int zOrder)
	{
		this.zOrder = zOrder;
	}

	public boolean matches(Tile other)
	{
		if (this == other)
		{
			return true;
		}
		if (other == null)
		{
			return false;
		}
		return this.getClass() == other.getClass();
	}

	@Override
	public void setLocation(Point p)
	{
		super.setLocation(p);

		this.x = p.x;
		this.y = p.y;
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		setOpaque(false);

		Graphics2D g2 = (Graphics2D)g;
		GradientPaint grad = new GradientPaint(0, 70, LightGreen, 70, 0, DarkGreen, true);
		g2.setPaint(grad);

		g.fillPolygon(sideTile);
		g.fillPolygon(topTile);

		GradientPaint gradTop = new GradientPaint(20, 70, TopLightColor, 70, 0, TopColor, false);

		g2.setPaint(gradTop);

		g.fillRect(20, 20, 70, 70);

		g.fillPolygon(sideInnerTile);
		g.fillPolygon(topInnerTile);

		g2.setPaint(Color.BLACK);

		g.drawPolygon(sideTile);
		g.drawPolygon(sideInnerTile);

		g.drawPolygon(topTile);
		g.drawPolygon(topInnerTile);

		g.drawRect(20, 20, 70, 70);
	}
}
