import javax.swing.*;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.FlowLayout;


public class Bamboo1Tile extends PictureTile
{
	private Image Bamboo1 = new ImageIcon(this.getClass().getResource("/images/Sparrow.png")).getImage();

	public Bamboo1Tile()
	{
		super("Bamboo 1");
	}

	public static void main(String[] args)
	{
		JFrame jframe = new JFrame();

		jframe.setLayout(new FlowLayout());
		jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		jframe.setTitle("Bamboo 1 Tile");

		jframe.add(new Bamboo1Tile());

		jframe.pack();
		jframe.setVisible(true);
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(Bamboo1, 25, 25, this);
	}

	@Override
	public String toString()
	{
		return "Bamboo 1";
	}
}
