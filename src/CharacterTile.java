import javax.swing.*;
import java.awt.Font;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.FontMetrics;


public class CharacterTile extends AbstractCharacterTile
{
	private String chineseCharSymbol;

	public CharacterTile(char symbol)
	{
		super(symbol);
		setToolTipText(toString());
		chineseCharSymbol = Tile.chineseUniChars.get(Character.toString(symbol));
	}

	public static void main(String[] args)
	{
		JFrame		jframe = new JFrame();
		JPanel		jpanel = new JPanel();
		JScrollPane	jscrollpane = new JScrollPane(jpanel);

		jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		jframe.setTitle("Character Tiles");
		jframe.add(jscrollpane);

		jpanel.add(new CharacterTile('1'));
		jpanel.add(new CharacterTile('2'));
		jpanel.add(new CharacterTile('3'));
		jpanel.add(new CharacterTile('4'));
		jpanel.add(new CharacterTile('5'));
		jpanel.add(new CharacterTile('6'));
		jpanel.add(new CharacterTile('7'));
		jpanel.add(new CharacterTile('8'));
		jpanel.add(new CharacterTile('9'));

		jframe.pack();
		jframe.setVisible(true);
	}

	public boolean matches(Tile other)
	{
		return super.matches(other) && symbol == ((CharacterTile)other).symbol;
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		g.setColor(Color.RED);
		g.drawString(Character.toString(symbol), 75, 35);

		if (symbol >= '1' && symbol <= '9')
		{
			drawSymbol(g, Color.BLACK);
		}
	}

	@Override
	public void drawSymbol(Graphics g, Color c)
	{
		Font font = g.getFont();
		FontMetrics fontMetric = g.getFontMetrics();
		int width = fontMetric.stringWidth(chineseCharSymbol);

		g.setColor(c);
		font = font.deriveFont(font.getSize2D() * 2F);
		g.setFont(font);
		g.drawString( chineseCharSymbol, (getWidth() - (width - 5)) / 2, (getHeight() + 100) / 4);

		g.setColor(Color.RED);
		g.drawString(Tile.chineseUniChars.get("wan"), (getWidth() - (width - 5)) / 2, (getHeight() + 55) / 2);
	}
}
