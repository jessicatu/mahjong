import java.awt.Color;
import java.awt.Graphics;

public abstract class AbstractCharacterTile extends Tile
{
	protected char symbol;
	
	public AbstractCharacterTile(char symbol)
	{
		this.symbol = symbol;
	}
	
	public abstract void drawSymbol(Graphics g, Color c);
	
	@Override
	public String toString()
	{
		switch (symbol)
		{
			case 'C':
				return "Red Dragon";
			case 'F': 
				return "Green Dragon";
			case 'N': 
				return "North Wind";
			case 'E': 
				return "East Wind";
			case 'W': 
				return "West Wind";
			case 'S': 
				return "South Wind";
			default: 
				return "Character " + symbol;
		}
	}
}
