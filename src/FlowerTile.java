import java.awt.Image;
import java.awt.Graphics;
import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.ImageIcon;


public class FlowerTile extends PictureTile
{
	private Image Chrysanthemum = new ImageIcon(this.getClass().getResource("/images/Chrysanthemum.png")).getImage();
	private Image Orchid 		= new ImageIcon(this.getClass().getResource("/images/Orchid.png")).getImage();
	private Image Plum 			= new ImageIcon(this.getClass().getResource("/images/Plum.png")).getImage();
	private Image Bamboo 		= new ImageIcon(this.getClass().getResource("/images/Bamboo.png")).getImage();
	
	public FlowerTile(String name)
	{
		super(name);
	}

	public static void main(String[] args)
	{
		JFrame jrame = new JFrame();

		jrame.setLayout(new FlowLayout());
		jrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jrame.setTitle("Flower Tiles");

		jrame.add(new FlowerTile("Chrysanthemum"));
		jrame.add(new FlowerTile("Orchid"));
		jrame.add(new FlowerTile("Plum"));
		jrame.add(new FlowerTile("Bamboo"));

		jrame.pack();
		jrame.setVisible(true);
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		switch (name)
		{
			case "Chrysanthemum":
				g.drawImage(Chrysanthemum, 22, 25, this);
				break;
			case "Orchid":
				g.drawImage(Orchid, 22, 25, this);
				break;
			case "Plum":
				g.drawImage(Plum, 28, 25, this);
				break;
			case "Bamboo":
				g.drawImage(Bamboo, 30, 25, this);
				break;
		}
	}
}
