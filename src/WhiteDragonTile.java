import java.awt.Color;
import java.awt.Stroke;
import java.awt.Graphics;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.BasicStroke;

import javax.swing.*;

public class WhiteDragonTile extends Tile
{
	public WhiteDragonTile()
	{
		setToolTipText(toString());
	}

	public static void main(String[] args)
	{
		JFrame jframe = new JFrame();

		jframe.setLayout(new FlowLayout());
		jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		jframe.setTitle("White Dragon Tile");

		jframe.add(new WhiteDragonTile());

		jframe.pack();
		jframe.setVisible(true);
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Stroke stroke = new BasicStroke(4, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL, 0, new float[] { 6, 14 }, 0);
		g.setColor(Color.RED);
		g.drawRect(28, 28, 54, 54);
		g.drawRect(32, 32, 46, 46);

		Graphics2D g2 = (Graphics2D)g;
		g2.setStroke(stroke);
		g2.drawRect(30, 30, 50, 50);
	}

	@Override
	public String toString()
	{
		return "White Dragon";
	}
}
