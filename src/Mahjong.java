import javax.swing.*;

public class Mahjong extends JFrame
{
    private Mahjong() {
        ImagePanel panel    = new ImagePanel();
        MahjongBoard board  = new MahjongBoard(panel, this);
        JMenuBar menu       = new MahjongMenuBar(board);

        setJMenuBar(menu);
        add(panel);

        pack();
        setVisible(true);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args)
    {
        new Mahjong();
    }
}
