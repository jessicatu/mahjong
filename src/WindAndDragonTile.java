import javax.swing.*;
import java.awt.Font;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.FontMetrics;

public class WindAndDragonTile extends AbstractCharacterTile
{
	private String chineseSymbol;

	public WindAndDragonTile(char symbol) 
	{
		super(symbol);
		chineseSymbol = Tile.chineseUniChars.get(Character.toString(symbol));
		setToolTipText(toString());
	}

	public static void main(String[] args)
	{
		JFrame		jframe = new JFrame();
		JPanel		jpanel = new JPanel();
		JScrollPane	jscrollpane = new JScrollPane(jpanel);

		jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		jframe.setTitle("Character Tiles");
		jframe.add(jscrollpane);

		jpanel.add(new WindAndDragonTile('N'));
		jpanel.add(new WindAndDragonTile('E'));
		jpanel.add(new WindAndDragonTile('W'));
		jpanel.add(new WindAndDragonTile('S'));
		jpanel.add(new WindAndDragonTile('C'));
		jpanel.add(new WindAndDragonTile('F'));
		jframe.pack();
		jframe.setVisible(true);
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		g.setColor(Color.RED);
		g.drawString(Character.toString(symbol), 75, 32);

		if (symbol == 'N' || symbol == 'E' || symbol == 'W' || symbol == 'S' )
		{
			drawSymbol(g, Color.BLACK);
		}

		if (symbol == 'C' )
		{
			drawSymbol(g, Color.RED);
		}

		if (symbol == 'F' )
		{
			drawSymbol(g, Color.GREEN);
		}
	}

	@Override
	public void drawSymbol(Graphics g, Color c)
	{
		Font font = g.getFont();
		FontMetrics fontMetrics = g.getFontMetrics();
		int width = fontMetrics.stringWidth(chineseSymbol);

		g.setColor(c);
		font = font.deriveFont(font.getSize2D() * 4.5F);
		g.setFont(font);
		g.drawString( chineseSymbol, (getWidth() - (width + 25) ) / 2, (getHeight() + 45) / 2);
	}
}
