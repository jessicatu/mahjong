import java.awt.Image;
import java.awt.Graphics;
import javax.swing.*;
import java.awt.FlowLayout;

public class SeasonTile extends PictureTile
{
	private Image Spring = new ImageIcon(this.getClass().getResource("/images/Spring.png")).getImage();
	private Image Summer = new ImageIcon(this.getClass().getResource("/images/Summer.png")).getImage();
	private Image Fall 	 = new ImageIcon(this.getClass().getResource("/images/Fall.png")).getImage();
	private Image Winter = new ImageIcon(this.getClass().getResource("/images/Winter.png")).getImage();

	public SeasonTile(String name)
	{
		super(name);
	}

	public static void main(String[] args)
	{
		JFrame jframe = new JFrame();

		jframe.setLayout(new FlowLayout());
		jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		jframe.setTitle("Season Tiles");

		jframe.add(new SeasonTile("Spring"));
		jframe.add(new SeasonTile("Summer"));
		jframe.add(new SeasonTile("Fall"));
		jframe.add(new SeasonTile("Winter"));

		jframe.pack();
		jframe.setVisible(true);
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		switch(name){
		case "Spring":
			g.drawImage(Spring, 25, 30, this);
			break;
		case "Summer":
			g.drawImage(Summer, 24, 25, this);
			break;
		case "Fall":
			g.drawImage(Fall, 25, 25, this);
			break;
		case "Winter":
			g.drawImage(Winter, 25, 25, this);
			break;
		}
	}
}
