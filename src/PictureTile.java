import javax.swing.*;
import java.awt.Graphics;
import java.awt.FlowLayout;

public abstract class PictureTile extends Tile
{
	protected String name;
	
	public PictureTile(String name)
	{
		this.name = name;
		setToolTipText(toString());
	}

	public static void main(String[] args)
	{
		JFrame jframe = new JFrame();

		jframe.setLayout(new FlowLayout());
		jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		jframe.setTitle("Picture Tiles");

		jframe.add(new Bamboo1Tile());

		jframe.add(new FlowerTile("Chrysanthemum"));
		jframe.add(new FlowerTile("Orchid"));
		jframe.add(new FlowerTile("Plum"));
		jframe.add(new FlowerTile("Bamboo"));

		jframe.add(new SeasonTile("Spring"));
		jframe.add(new SeasonTile("Summer"));
		jframe.add(new SeasonTile("Fall"));
		jframe.add(new SeasonTile("Winter"));

		jframe.pack();
		jframe.setVisible(true);
	}

	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
	}

	@Override
	public String toString()
	{
		return name;
	}
}
