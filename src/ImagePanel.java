import java.awt.*;
import javax.swing.*;

public class ImagePanel extends JPanel {
	private int dragonBackgroundWidth;
	private int dragonBackgroundHeight;
	private Image dragonBackground = new ImageIcon(this.getClass().getResource("/images/dragon_bg.png")).getImage();
	
	public ImagePanel() {
		this.setLayout(null);
		this.setPreferredSize(new Dimension(1366, 700));
		this.dragonBackgroundWidth = dragonBackground.getWidth(this) / 2;
		this.dragonBackgroundHeight = dragonBackground.getHeight(this) / 2;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (dragonBackground != null) {
			int x = this.getParent().getWidth() / 2 - dragonBackgroundWidth;
			int y = this.getParent().getHeight() / 2 - dragonBackgroundHeight;
			
			this.setBackground(Color.YELLOW);
			g.drawImage(dragonBackground, x, y, this);
		}
	}
}
