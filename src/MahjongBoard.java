import java.awt.*;
import javax.swing.*;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import java.util.Collections;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MahjongBoard implements MouseListener
{
    private int         seed;
    private ImagePanel  panel;
    private JFrame      frame;
    private boolean     soundStatus = true;
    private boolean     tournamentMode = false;
    private List<Tile>  tiles       = new ArrayList<>();
    private List<Point> points      = new ArrayList<>();
    private List<Tile>  usedTiles   = new ArrayList<>();
    private List<Tile>  activeTiles = new ArrayList<>();


    public MahjongBoard(ImagePanel panel, JFrame frame)
    {
        this.panel = panel;
        this.frame = frame;
        setSeed((int)(System.currentTimeMillis() % 1000000));
    }

    private void setTiles()
    {
        for (char c : new char[] {'1', '2', '3', '4', '5', '6', '7', '8', '9'})
        {
            for (int i = 0; i < 4; i++)
            {
                tiles.add(new CharacterTile(c));
            }
        }
        for (char c : new char[] {'N', 'E', 'W', 'S'})
        {
            for (int i = 0; i < 4; i++)
            {
                tiles.add(new WindAndDragonTile(c));
            }
        }
        for (int i : new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9})
        {
            for (int j = 0; j < 4; j++)
            {
                tiles.add(new CircleTile(i));

                if (i < 4) {
                    tiles.add(new Bamboo1Tile());
                } else {
                    tiles.add(new BambooTile(i));
                }
            }
        }
        for (char c : new char[] {'C', 'F'})
        {
            for (int i = 0; i < 4; i++)
            {
                tiles.add(new WindAndDragonTile(c));
            }
        }
        for (int i = 0; i < 4; i++)
        {
            tiles.add(new WhiteDragonTile());
        }
        for (String s : new String[] {"Chrysanthemum", "Orchid", "Plum", "Bamboo"})
        {
            tiles.add(new FlowerTile(s));
        }
        for (String s : new String[] {"Spring", "Summer", "Fall", "Winter"})
        {
            tiles.add(new SeasonTile(s));
        }
        Collections.shuffle(tiles, new Random(seed));
    }

    private void setPoints()
    {
        int[] x = { 280, 350, 420, 490, 560, 630, 700, 770, 840, 910, 980, 1050, 420, 490, 560, 630, 700, 770, 840, 910,
                350, 420, 490, 560, 630, 700, 770, 840, 910, 980, 210, 280, 350, 420, 490, 560, 630, 700, 770, 840, 910,
                980, 1050, 280, 350, 420, 490, 560, 630, 700, 770, 840, 910, 980, 1050, 1120, 1190, 350, 420, 490, 560,
                630, 700, 770, 840, 910, 980, 420, 490, 560, 630, 700, 770, 840, 910, 280, 350, 420, 490, 560, 630, 700,
                770, 840, 910, 980, 1050, 510, 580, 650, 720, 790, 860, 510, 580, 650, 720, 790, 860, 510, 580, 650, 720,
                790, 860, 510, 580, 650, 720, 790, 860, 510, 580, 650, 720, 790, 860, 510, 580, 650, 720, 790, 860, 600,
                670, 740, 810, 600, 670, 740, 810, 600, 670, 740, 810, 600, 670, 740, 810, 690, 760, 690, 760, 745 };
        int[] y = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70, 70, 70, 70, 70, 70, 70, 70, 140, 140, 140, 140, 140, 140, 140,
                140, 140, 140, 245, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 280, 280, 280, 280, 280,
                280, 280, 280, 280, 280, 280, 280, 245, 245, 350, 350, 350, 350, 350, 350, 350, 350, 350, 350, 420, 420,
                420, 420, 420, 420, 420, 420, 490, 490, 490, 490, 490, 490, 490, 490, 490, 490, 490, 490, 90, 90, 90, 90,
                90, 90, 160, 160, 160, 160, 160, 160, 230, 230, 230, 230, 230, 230, 300, 300, 300, 300, 300, 300, 370, 370,
                370, 370, 370, 370, 440, 440, 440, 440, 440, 440, 180, 180, 180, 180, 250, 250, 250, 250, 320, 320, 320, 320,
                390, 390, 390, 390, 270, 270, 340, 340, 320 };
        for (int i = 0; i < 144; i++) {
            points.add(new Point(x[i], y[i]));
        }
    }

    private void setTileLocation()
    {
        for (Tile t : tiles)
        {
            t.setLocation(points.get(tiles.indexOf(t)));
        }
    }

    private void addMouseListener(MouseListener mouseListener)
    {
        for (Tile t : tiles)
        {
            t.addMouseListener(mouseListener);
        }
    }

    public void drawMahjongBoard()
    {
        setTiles();
        setPoints();
        setTileLocation();
        addMouseListener(this);

        // Layer 4
        panel.add(tiles.get(143));
        tiles.get(143).setLayer(4);

        for (int i = 139; i < 143; i++) // Layer 3
        {
            panel.add(tiles.get(i));
            tiles.get(i).setLayer(3);
        }
        for (int i = 123; i < 139; i++) // Layer 2
        {
            panel.add(tiles.get(i));
            tiles.get(i).setLayer(2);
        }
        for (int i = 87; i < 123; i++)  // Layer 1
        {
            panel.add(tiles.get(i));
            tiles.get(i).setLayer(1);
        }
        for (int i = 0; i < 87; i++)    // Layer 0
        {
            panel.add(tiles.get(i));
            tiles.get(i).setLayer(0);
        }
        for (Tile t : tiles)
        {
            t.setOpaque(false);
        }
        panel.repaint();
    }

    public void clearTiles()
    {
        for (Tile t : tiles)
        {
            panel.remove(t);
        }

        tiles.clear();
        usedTiles.clear();
        activeTiles.clear();

        panel.repaint();
    }

    private boolean isOpen(Tile t)
    {
        int id = tiles.indexOf(t);
        int x = t.getXPoint();
        int y = t.getYPoint();

        if (id == 0 || id == tiles.size() - 1)
        {
            return true;
        }
        else
        {
            int layer;
            boolean above = false, sides = false;
            Tile leftTile, rightTile;

            layer       = tiles.get(id).getLayer();
            leftTile    = tiles.get(id - 1);
            rightTile   = tiles.get(id + 1);

            for (Tile temp : tiles)
            {
                if ((layer + 1 == temp.getLayer()) && x + 20 == temp.getXPoint() && y + 20 == temp.getYPoint())
                {
                    above = true;
                }

                if (layer == 3)
                {
                    for (Tile temp2 : tiles)
                    {
                        if (temp2.getXPoint() == 745 && temp2.getYPoint() == 320)
                        {
                            above = true;
                        }
                    }
                }
            }

            if (x == 280 && y == 280)
            {
                for (Tile temp : tiles)
                {
                    if (temp.getXPoint() == 210 && temp.getYPoint() == 245)
                    {
                        sides = (true && x + 70 == rightTile.getXPoint());
                    }
                }
            }
            else if (x == 1050 && y == 210)
            {
                for (Tile temp : tiles)
                {
                    if (temp.getXPoint() == 1120 && temp.getYPoint() == 245)
                    {
                        sides = (x - 70 == leftTile.getXPoint() && true);
                    }
                }
            }
            else
            {
                sides = (x - 70 == leftTile.getXPoint() && x + 70 == rightTile.getXPoint());
            }

            return !above && !sides;
        }
    }

    public List<Tile> getTiles()
    {
        return this.tiles;
    }

    public List<Tile> getUsedTiles()
    {
        return this.usedTiles;
    }

    public ImagePanel getPanel()
    {
        return this.panel;
    }

    public boolean getSoundStatus()
    {
        return this.soundStatus;
    }

    public void setSeed(int seed)
    {
        this.seed = seed;
        this.setTitle("Mahjong (Seed: " + this.seed + ")");
    }

    public void setTitle(String title)
    {
        this.frame.setTitle(title);
    }

    public void setTournamentMode(boolean tournamentMode)
    {
        this.tournamentMode = tournamentMode;
    }

    public void setSoundStatus(boolean soundStatus)
    {
        this.soundStatus = soundStatus;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent)
    {
        if (isOpen((Tile)mouseEvent.getSource()))
        {
            Tile t = (Tile)mouseEvent.getSource();
            activeTiles.add(t);
            t.setBorder(BorderFactory.createDashedBorder(Color.red,3.0f, 3.5f, 3.5f, false));
        }
        else
        {
            if (getSoundStatus())
            {
                PlayClip clip = new PlayClip("/audio/not_open.wav");
                clip.play();
            }
        }

        if (activeTiles.size() == 2)
        {
            Tile t1 = activeTiles.get(0);
            Tile t2 = activeTiles.get(1);

            t1.setBorder(null);
            t2.setBorder(null);

            if (t1 == t2)
            {
                activeTiles.clear();
            }
            else
            {
                if (t1.matches(t2))
                {
                    t1.setZOrder(panel.getComponentZOrder(t1));
                    t2.setZOrder(panel.getComponentZOrder(t2));

                    usedTiles.add(t1);
                    usedTiles.add(t2);

                    tiles.remove(t1);
                    tiles.remove(t2);

                    panel.remove(t1);
                    panel.remove(t2);

                    panel.repaint();

                    activeTiles.clear();

                    if (getSoundStatus())
                    {
                        PlayClip clip = new PlayClip("/audio/removed.wav");
                        clip.play();
                    }

                    if (tiles.size() == 0)
                    {
                        Fireworks fireworks = new Fireworks(panel);
                        fireworks.setSound(getSoundStatus());
                        fireworks.setExplosions(10,1000);
                        fireworks.fire();

                        JOptionPane.showMessageDialog(panel, "Congratulations, you've won!\n\nYou can start a new game by clicking Game > Restart to play a game with a new Seed, or to play this game again.", "You Won!", JOptionPane.INFORMATION_MESSAGE);
                    }

                    if (tournamentMode) {
                        this.setTitle("Mahjong | Seed: " + this.seed + " | Score: " + usedTiles.size() + "");
                    }
                }
                else
                {
                    activeTiles.clear();
                }
            }
        }
    }
    @Override
    public void mouseExited(MouseEvent mouseEvent) { }
    @Override
    public void mouseReleased(MouseEvent mouseEvent) { }
    @Override
    public void mousePressed(MouseEvent mouseEvent) { }
    @Override
    public void mouseEntered(MouseEvent mouseEvent) { }
}
